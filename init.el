(require 'package)
(setq package-enable-at-startup nil)
;(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(package-initialize)
;;temporary fix to elpa bad request bug

(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))
