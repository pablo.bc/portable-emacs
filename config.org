#+title: Emacs Config
* Install Use-package
#+BEGIN_SRC emacs-lisp
 (unless (package-installed-p 'use-package)
   (package-refresh-contents)
   (package-install 'use-package))

 (require 'use-package-ensure)
 (setq use-package-always-ensure t)
#+END_SRC
* Quelpa
Enables =quelpa= to download packages from source. Also includes a use-package wrapper for it.
#+BEGIN_SRC emacs-lisp
  (use-package quelpa-use-package)	;
#+END_SRC
* Minor conveniences
** Fullscreen
#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;;(set-frame-parameter nil 'fullscreen 'maximized)
#+END_SRC
** No backups
#+BEGIN_SRC emacs-lisp
(setq make-backup-files nil)
#+END_SRC
** Visit config
Quickly edit =~/.emacs.d/config.org=
#+BEGIN_SRC emacs-lisp
  (defun config-visit ()
    (interactive)
    (find-file "~/.emacs.d/config.org"))
  (global-set-key (kbd "C-c e") 'config-visit)
#+END_SRC
** Load config
Simply pressing =Control-c r= will reload this file.
You can also manually invoke =config-reload=.
#+BEGIN_SRC emacs-lisp
  (defun config-reload ()
    "Reloads ~/.emacs.d/config.org at runtime"
    (interactive)
    (org-babel-load-file (expand-file-name "~/.emacs.d/config.org")))
  (global-set-key (kbd "C-c r") 'config-reload)
#+END_SRC
** Remove DOS eol
#+BEGIN_SRC emacs-lisp
(defun my/remove-dos-eol ()
  "Do not show ^M in files containing mixed UNIX and DOS line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))
#+END_SRC
** Disable graphical menu
#+BEGIN_SRC emacs-lisp
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(add-to-list 'default-frame-alist
             '(vertical-scroll-bars . nil))
#+END_SRC
** Hide dot-files in dired
#+BEGIN_SRC emacs-lisp
  (use-package dired-hide-dotfiles
  :config
  (dired-hide-dotfiles-mode)
  (define-key dired-mode-map "." 'dired-hide-dotfiles-mode))
#+END_SRC
** Coding system
Prefer utf-8
#+BEGIN_SRC emacs-lisp
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; backwards compatibility as default-buffer-file-coding-system
;; is deprecated in 23.2.
(if (boundp 'buffer-file-coding-system)
    (setq-default buffer-file-coding-system 'utf-8)
  (setq default-buffer-file-coding-system 'utf-8))

;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))
#+end_SRC
** Diminish minor modes
 #+BEGIN_SRC emacs-lisp
    (use-package diminish)
    (diminish 'visual-line-mode)
 #+END_SRC
** Replace yes-or-no questions
#+BEGIN_SRC emacs-lisp
(defalias 'yes-or-no-p 'y-or-n-p)
#+END_SRC
** Ignore alarms
#+begin_src emacs-lisp
(setq ring-bell-function 'ignore)
#+end_src
** Use git-bash as shell
#+begin_src emacs-lisp
(setq explicit-shell-file-name (concat %~dp0 "Programs/Git/bin/bash.exe"))
(setq shell-file-name explicit-shell-file-name)
(setq explicit-bash.exe-args '("--login" "-i"))
(add-to-list 'exec-path (concat %~dp0 "Programs/Git/bin"))
#+end_src
** Set font
#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist '(font . "Consolas"))
#+end_src
** Remove startup message
#+BEGIN_SRC emacs-lisp
(setq inhibit-startup-message t)
#+END_SRC
** Hl current line
Only when Emacs is in GUI mode.
#+BEGIN_SRC emacs-lisp
(when window-system (global-hl-line-mode t))
#+END_SRC
* Editing
Allows to move lines or regions up and down.
#+BEGIN_SRC emacs-lisp
   (use-package move-text
   :bind ("M-n" . move-text-down)
   :bind ("M-p" . move-text-up))
#+END_SRC
#+BEGIN_SRC emacs-lisp
(show-paren-mode 1)
#+END_SRC
** Electric mode
Typing the first character in a set of 2, completes the second one after your cursor.
You can easily add and remove pairs yourself, have a look.
#+BEGIN_SRC emacs-lisp
  (setq electric-pair-pairs '(
                             (?\{ . ?\})
                             (?\( . ?\))
                             (?\[ . ?\])
                             (?\" . ?\")
                             ))
  (add-hook 'prog-mode-hook 'electric-pair-local-mode)
#+END_SRC
 #+BEGIN_SRC emacs-lisp
 (setq scroll-conservatively 300)
 #+END_SRC
**  Enable =which-key= package
 #+BEGIN_SRC emacs-lisp
   (use-package which-key
     :diminish which-key-mode
     :init
     (which-key-mode))
 #+END_SRC
** =Avy=
#+BEGIN_SRC emacs-lisp
   (use-package avy
     :bind
       ("M-s" . avy-goto-char))
 #+END_SRC
** Multiple cursors
Installs =multiple-cursors= and binds a few keys.
#+BEGIN_SRC emacs-lisp
   (use-package multiple-cursors

     :bind (("C-S-l" . 'mc/edit-lines)
            ("C->"  . 'mc/mark-next-like-this)
            ("C-<" . 'mc/mark-previous-like-this)))
#+END_SRC
* Helm
#+BEGIN_SRC emacs-lisp
   (use-package helm
     :diminish helm-mode
     :bind (("M-x" . #'helm-M-x)
            ("C-x C-f" . #'helm-find-files)
            ("C-o" . #'helm-occur)
            ("M-y" . #'helm-show-kill-ring)
            ("C-x b" . #'helm-mini))
     :config
     (setq helm-split-window-in-side-p t
           helm-move-to-line-cycle-in-source nil))

   (use-package helm-projectile
     :diminish helm-projectile
     :config
     (projectile-global-mode)
     (setq projectile-completion-system 'helm)
     (helm-projectile-on))

   ;;Depends on expac
  ;; (use-package helm-system-packages)
   (helm-mode 1)
#+END_SRC
* Spelling
Enable =flyspell= and =flyspell-prog-mode= to highlight spelling errors.
#+begin_src emacs-lisp
;  (add-hook 'text-mode-hook #'flyspell-mode)

;  (add-hook 'prog-mode-hook #'flyspell-prog-mode)
#+end_src
* Workspaces
#+begin_src emacs-lisp
   (use-package perspective
   :init (persp-mode))
#+end_src
* Windows
#+BEGIN_SRC emacs-lisp
  (defun split-and-follow-horizontally ()
    (interactive)
    (split-window-below)
    (balance-windows)
    (other-window 1))
  (global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

  (defun split-and-follow-vertically ()
    (interactive)
    (split-window-right)
    (balance-windows)
    (other-window 1))
  (global-set-key (kbd "C-x 3") 'split-and-follow-vertically)
#+END_SRC
** Switch windows
#+BEGIN_SRC emacs-lisp
  (use-package switch-window    
    :config
      (setq switch-window-input-style 'minibuffer)
      (setq switch-window-increase 4)
      (setq switch-window-threshold 2)
      (setq switch-window-shortcut-style 'qwerty)
      (setq switch-window-qwerty-shortcuts
            '("a" "s" "d" "f" "j" "k" "l" "i" "o" "p"))
      :bind
      ([remap other-window] . switch-window))
#+END_SRC
* Theme
Using a modified Flatui theme
#+BEGIN_SRC emacs-lisp
  (use-package flatui-theme
    :init
    (load-theme 'flatui t)
    :config
    (custom-theme-set-faces
     'flatui
     '(org-document-info-keyword ((t (:foreground "#808080" :slant oblique))))
     '(org-document-title ((t (:foreground "#34495e" :weight bold))))
     '(mode-line ((t (:weight bold :foreground "#2492db" :background "#34495e" :box (:line-width 4 :color "#34495e")))))))
     ;; '(org-level-1 ((t (:height 1.1 :weight bold :underline t :foreground "#313C7A"))))
     ;; '(org-level-2 ((t (:height 1.0 :weight bold :foreground "#20903A"))))
     ;; '(org-level-3 ((t (:height 1.0 :weight bold :foreground "#E44F48"))))
     ;; '(org-level-4 ((t (:height 1.0 :foreground "#9441B2"))))
     ;; '(org-ellipsis ((t ( :foreground "#6B89940"))))
     ;; '(org-block-begin-line ((t (:background "unspecified" :foreground "#808080" :slant italic))))
     ;; '(show-paren-match ((t (:background "#979996" :foreground "#EDEEEB" :weight bold))))
     ;; '(linum-relative-current-face ((t (:background "#928982" :foreground "#F5EBE1" :weight bold))))
     ;; '(elfeed-search-unread-title-face ((t (:foreground "#383e3f" :weight bold :height 1.0))))
     ;; '(org-agenda-structure ((t (:foreground "#4C7A90" :weight bold :height 1.1))))
     ;; '(mode-line-inactive ((t (:weight bold :foreground "#867e78" :background "#F3E7D3" :box (:line-width 4 :color "#F0DFCA")))))
     ;; '(mode-line-buffer-id ((t (:weight bold :foreground "#B89A88"))))
     ;; '(mode-line-buffer-id-inactive ((t (:weight medium :foreground "#B89A88"))))))
#+end_src
** Modeline
#+BEGIN_SRC emacs-lisp
  (use-package powerline
    :config
    (powerline-default-theme)
    (setq powerline-default-separator  'contour))
  ;; (use-package doom-modeline
    ;; :init (doom-modeline-mode 1))

#+END_SRC
* Org
Install org-contrib
#+BEGIN_SRC emacs-lisp
   (use-package org
    :ensure org-plus-contrib)
#+END_SRC
** Org priorities
Adds custom colors to org priorities and adds /D/ as the lowest priority.
#+begin_src emacs-lisp
  (setq org-priority-faces '((65 :foreground "#E71E1E" :weight bold :underline t)
                             (66 :foreground "#FFA900" :weight bold)
                             (67 :foreground "#0099FF" :weight bold :slant italic)
                             (68 :foreground "#B900FF" :slant italic))
        org-lowest-priority 68)
#+end_src
*** Keywords
#+begin_src emacs-lisp
  (setq org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "INPROGRESS(i)" "|" "DONE(d)" "CANCELED(c)"))
        org-todo-keyword-faces 
        '(("TODO" :foreground "#F31223" :weight bold)
          ("NEXT" :foreground "#0097FF" :weight bold :slant italic)
          ("INPROGRESS" :foreground "#FFB100" :slant italic)
          ("DONE" :foreground "#7AB286" :weight bold :slant italic)
          ("CANCELED" :foreground "#A677B1" :weight normal :slant italic)))
#+end_src
*** Visual line
#+BEGIN_SRC emacs-lisp
(add-hook 'org-mode-hook
	    '(lambda ()
	       (visual-line-mode 1)))
#+END_SRC
*** Indent headings
#+BEGIN_SRC emacs-lisp
  (add-hook 'org-mode-hook 'org-indent-mode)
#+END_SRC
*** Latex preview
Render it a little bit bigger
#+begin_src emacs-lisp
(setq org-format-latex-options '(:foreground default :background default :scale 1.4 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers ("begin" "$1" "$" "$$" "\\(" "\\[")))
#+end_src
*** Shift selection
#+BEGIN_SRC emacs-lisp
  (setq org-support-shift-select t)
#+END_SRC
** Open code block in current window
#+BEGIN_SRC emacs-lisp
  (setq org-src-window-setup 'current-window)
#+END_SRC
** Org templates
#+begin_src emacs-lisp
(require 'org-tempo)
#+end_src
* Magit
#+begin_src emacs-lisp
(use-package magit)
#+end_src
